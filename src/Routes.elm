module Routes exposing (Route(..), routes, urlToRoute, viewNotFound)

import Html exposing (..)
import Html.Attributes exposing (..)
import Url exposing (Url)
import Url.Parser as UP exposing (..)


type Route
    = NotFound
    | Root


routes : Parser (Route -> a) a
routes =
    oneOf
        [ UP.map Root UP.top
        ]


urlToRoute : Url.Url -> Route
urlToRoute url =
    Maybe.withDefault NotFound (UP.parse routes url)


viewNotFound : String -> Html msg
viewNotFound s =
    p [] [ text ("Page not found:" ++ s) ]
