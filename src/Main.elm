module Main exposing (Model, init, main)

import Browser
import Browser.Navigation as Nav
import Html exposing (..)
import Http
import Page.Root as Root
import Routes
import Sessions
import Time
import Url exposing (Url)
import Url.Parser as UP exposing (..)


main =
    Browser.application
        { init = init
        , onUrlChange = UrlChanged
        , onUrlRequest = LinkClicked
        , update = update
        , view = view
        , subscriptions = subscriptions
        }



-- MODEL


type alias Model =
    { name : String
    , url : Url.Url
    , key : Nav.Key
    , route : Routes.Route
    , session_ping : Int
    , session : Sessions.Session
    , root_model : Root.Model
    }



-- MESSAGES


type Msg
    = UrlChanged Url.Url
    | NoOp
    | LinkClicked Browser.UrlRequest
    | SessionPinged Time.Posix
    | GotPing (Result Http.Error ())
    | RootMsg Root.Msg



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Time.every (toFloat model.session_ping) SessionPinged



-- INIT


init : () -> Url.Url -> Nav.Key -> ( Model, Cmd msg )
init _ url key =
    ( { name = "Baselm"
      , url = url
      , route = Routes.urlToRoute url
      , key = key
      , session_ping = 1000
      , session = Sessions.new
      , root_model = Root.init
      }
    , Cmd.none
    )



-- UPDATE


update : Msg -> Model -> ( Model, Cmd Msg )
update message model =
    case message of
        NoOp ->
            ( model, Cmd.none )

        UrlChanged url ->
            ( { model | url = url }, Cmd.none )

        SessionPinged t ->
            ( model, ping )

        LinkClicked req ->
            case req of
                Browser.Internal url ->
                    case Maybe.withDefault Routes.NotFound (UP.parse Routes.routes url) of
                        Routes.NotFound ->
                            ( model, Cmd.none )

                        Routes.Root ->
                            ( model, Cmd.none )

                Browser.External url ->
                    ( model, Nav.load url )

        GotPing res ->
            case res of
                Ok _ ->
                    ( model, Cmd.none )

                Err _ ->
                    ( model, Cmd.none )

        RootMsg sub ->
            let
                ( subModel, subCmd ) =
                    Root.update sub model.session model.root_model
            in
            ( { model | root_model = subModel }
            , Cmd.map RootMsg subCmd
            )



-- VIEW


view : Model -> Browser.Document Msg
view model =
    let
        title =
            model.name

        body =
            case model.route of
                Routes.NotFound ->
                    --- Modify NotFound page
                    Routes.viewNotFound (Url.toString model.url)

                Routes.Root ->
                    Html.map RootMsg (Root.view model.root_model model.session)
    in
    { title = title, body = [ body ] }



-- Ping to server


ping : Cmd Msg
ping =
    Http.get
        { url = "/api/ping"
        , expect = Http.expectWhatever GotPing
        }
