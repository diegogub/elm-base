module Sessions exposing (Session, new)

import Dict exposing (Dict)


type alias Session =
    { id : String
    , user : String
    , data : Dict String String
    , valid : Bool
    }


new : Session
new =
    Session "" "" Dict.empty False
