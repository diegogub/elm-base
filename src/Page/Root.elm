module Page.Root exposing (Model, Msg(..), init, update, view)

import Html exposing (..)
import Html.Events exposing (onClick)
import Sessions



-- Model


type alias Model =
    { counter : Int
    }



-- Init


init : Model
init =
    { counter = 0 }



-- Messages


type Msg
    = IncCountClicked



--UPDATE


update : Msg -> Sessions.Session -> Model -> ( Model, Cmd Msg )
update msg session model =
    case msg of
        IncCountClicked ->
            ( { model | counter = model.conuter + 1 }, Cmd.none )



--VIEW


view : Model -> Sessions.Session -> Html Msg
view model session =
    div []
        [ p []
            [ text (String.fromInt model.counter) ]
        , button
            [ onClick IncCountClicked ]
            [ text "inc" ]
        ]
